import React, { useState, useEffect } from "react";
import img1 from "./static/images/login-photo.jpg";
import "./Signup.css";
import { NavLink, useNavigate } from "react-router-dom";
import { useSignupMutation } from "./store/WorkoutsApi";

function Signup() {
  const navigate = useNavigate();
  const [signup, signupResponse] = useSignupMutation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [weight, setWeight] = useState("");

  useEffect(() => {
    if (signupResponse.isSuccess) {
      navigate("/");
    }
  }, [signupResponse]);

  const handleSubmit = (e) => {
    e.preventDefault();
    signup({ username, password, weight });
  };

  return (
    <div className="main-signup-container">
      <div className="signup-container">
        <div className="signup-image-container">
          <img className="signup-image" src={img1}></img>
        </div>
        <div className="signup-form-container">
          <h3>Sign Up</h3>
          <form onSubmit={handleSubmit} className="signup-form">
            <input
              placeholder="Username"
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            ></input>
            <input
              placeholder="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            ></input>
            <input
              placeholder="Starting Weight (lbs)"
              type="text"
              value={weight}
              onChange={(e) => setWeight(e.target.value)}
            ></input>
            <button
              onClick={() => signup}
              className="login-button"
              type="submit"
            >
              Create account
            </button>
            <div>
              <p>
                Already have an account? <NavLink to="/login">Login</NavLink>
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Signup;
