from fastapi.testclient import TestClient
from main import app
from queries.weights import WeightRepo
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_acccount_data():
    return {
        "id": 1,
        "username": "fakeuser"
    }


class FakeWeightRepo():
    def get_weights(self, account_data: dict):
        return [
            {
                "id": 1,
                "weight": 505,
                "date": "2023-08-25"
            },
        ]


def test_get_weights():
    app.dependency_overrides[WeightRepo] = FakeWeightRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_acccount_data
    response = client.get("/weights")
    data = response.json()

    assert response.status_code == 200
    assert data == [
            {
                "id": 1,
                "weight": 505,
                "date": "2023-08-25"
            },
        ]
