import { useCreateNoteMutation } from "./store/WorkoutsApi";
import React, { useState } from "react";
import "./CreateNote.css";

function CreateNote() {
  const [createNote] = useCreateNoteMutation();
  const [noteValue, setNoteValue] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    createNote({ content: noteValue });
  };

  const handleNoteValueChange = (event) => {
    setNoteValue(event.target.value);
  };

  return (
    <div>
      <h3>Create a Note</h3>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Note"
          value={noteValue}
          onChange={handleNoteValueChange}
        />
        <button className="create-note-button" type="submit">
          Create
        </button>
      </form>
    </div>
  );
}

export default CreateNote;
