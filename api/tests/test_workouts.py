from fastapi.testclient import TestClient
from main import app
from queries.workouts import WorkoutRepo
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_acccount_data():
    return {
        "id": 1,
        "username": "fakeuser"
    }


class FakeWorkoutRepo():
    def get_workouts(self, account_data: dict):
        return [
            {
                "id": 1,
                "name": "this is a test name",
                "exercises": ["exercise name", "test exercise"]
            },
            ]


def test_get_workouts():
    app.dependency_overrides[WorkoutRepo] = FakeWorkoutRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_acccount_data
    response = client.get("/workouts")
    data = response.json()

    assert response.status_code == 200
    assert data == [
            {
                "id": 1,
                "name": "this is a test name",
                "exercises": ["exercise name", "test exercise"]
            },
        ]
