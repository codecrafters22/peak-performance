from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from authenticator import authenticator
from routers import accounts, notes, apis, weights, workouts, goals

tags_metadata = [
    {
        "name": "Users",
        "description":
        "Operations with users. Login/Log out logic is also here."
    },
    {
        "name": "Notes",
        "description": "Operations with notes."
    },
    {
        "name": "Weights",
        "description": "Operations with weights."
    },
    {
        "name": "3rd Party APIs",
        "description": "Operations with 3rd party APIs."
    },
    {
        "name": "Workouts",
        "description": "Operations with workouts."
    },
    {
        "name": "Goals",
        "description": "Operations with goals."
    }
]

app = FastAPI(openapi_tags=tags_metadata)
app.include_router(authenticator.router, tags=["Users"])
app.include_router(accounts.router, tags=["Users"])
app.include_router(notes.router, tags=["Notes"])
app.include_router(weights.router, tags=["Weights"])
app.include_router(apis.router, tags=["3rd Party APIs"])
app.include_router(workouts.router, tags=["Workouts"])
app.include_router(goals.router, tags=["Goals"])

origins = [
    "http://localhost:8000",
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:3000"),
        "http://localhost:8000",
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
