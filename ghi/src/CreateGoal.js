import React, { useState } from "react";
import { useCreateGoalMutation } from "./store/WorkoutsApi";
import { useDispatch } from "react-redux";
import {  closeModal } from "./store/modalSlice";

function CreateGoal() {
  const [goalContent, setGoalContent] = useState("");
  const [createGoal] = useCreateGoalMutation();
  const dispatch = useDispatch();

  const handleGoalContentChange = (event) => {
    setGoalContent(event.target.value);
  };


  const handleSubmit = (e) => {
    e.preventDefault();
    createGoal({ content: goalContent });
    dispatch(closeModal());
  };

  return (
    <div>
      <h3>Create a Goal</h3>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Enter Goal Here"
          value={goalContent}
          onChange={handleGoalContentChange}
        />
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default CreateGoal;
