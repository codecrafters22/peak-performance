from fastapi.testclient import TestClient
from main import app
from queries.notes import NotesRepo, NoteIn
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_acccount_data():
    return {
        "id": 1,
        "username": "fakeuser"
    }


class FakeNotesRepo():
    def get_notes(self, account_data: dict):
        return [
            {
                "id": 1,
                "content": "this is a unit test"
            },
        ]

    def create(self, note: NoteIn, account_data: dict):
        new_note = {
            "id": 1,
            "user_id": account_data["id"],
            "content": note.content
        }
        return new_note


def test_create():
    app.dependency_overrides[NotesRepo] = FakeNotesRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_acccount_data
    body = {
        "content": "this is a unit test"
    }
    response = client.post("/notes", json=body)
    data = response.json()
    assert response.status_code == 200
    assert data == {"id": 1, "content": "this is a unit test"}


def test_get_notes():
    app.dependency_overrides[NotesRepo] = FakeNotesRepo
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_acccount_data
    response = client.get("/notes")
    data = response.json()

    assert response.status_code == 200
    assert data == [
            {
                "id": 1,
                "content": "this is a unit test"
            },
        ]
