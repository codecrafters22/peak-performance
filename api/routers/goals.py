from authenticator import authenticator
from fastapi import APIRouter, Depends
from typing import List
from queries.goals import GoalIn, GoalOut, GoalRepo


router = APIRouter()


@router.post("/goals", response_model=GoalOut)
async def create_goal(
    goal: GoalIn,
    repo: GoalRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(goal, account_data)


@router.get("/goals", response_model=List[GoalOut])
async def get_goals(
    goals: GoalRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return goals.get_goals(account_data)


@router.delete("/goals/{goal_id}", response_model=bool)
async def delete(
    goal_id: int,
    repo: GoalRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(goal_id)


@router.put("/goals/{goal_id}", response_model=GoalOut)
def complete_goal(
    goal_id: int,
    goal: GoalIn,
    repo: GoalRepo = Depends(),
) -> GoalOut:
    return repo.complete(goal_id)
