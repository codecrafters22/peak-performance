import React, { useState } from "react";
import "./ManageWorkouts.css";
import {
  useGetWorkoutsForAccountQuery,
  useDeleteWorkoutMutation,
} from "./store/WorkoutsApi";
import CreateWorkout from "./CreateWorkout";
import UpdateWorkout from "./UpdateWorkout";
import Modal from "react-modal";
import { useSelector, useDispatch } from "react-redux";
import { openModal, closeModal } from "./store/modalSlice";
import { openEditModal, closeEditModal } from "./store/editmodalSlice";
import { setSelectedWorkoutToEdit } from "./store/workoutSlice";

const customStyles = {
  overlay: {
    backgroundColor: "rgba(160, 160, 160, 0.75)",
  },
  content: {
    width: "50%",
    height: "50%",
    top: "40%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

Modal.setAppElement("#root");

function ManageWorkouts() {
  const { data: workouts, isLoading } = useGetWorkoutsForAccountQuery();

  const [selectedWorkout, setSelectedWorkout] = useState("");
  const [deleteWorkout] = useDeleteWorkoutMutation();
  const modal = useSelector((state) => state.modal.value);
  const editModal = useSelector((state) => state.editModal.value);
  const dispatch = useDispatch();

  const handleWorkoutChange = (event) => {
    const value = event.target.value;
    setSelectedWorkout(value);
  };

  const handleDeleteWorkout = (workout_id) => {
    deleteWorkout(workout_id);
  };

  const handleEditWorkout = (workout) => {
    dispatch(setSelectedWorkoutToEdit(workout));
    dispatch(openEditModal());
  };

  if (isLoading) return <div>Loading...</div>;

  if (workouts.length >= 1) {
    return (
      <>
        <div className="workouts-container">
          <div className="title-container">
            <div className="main-title">Manage Workouts</div>
          </div>
          <div className="workout-selection-container">
            <div>
              <button
                className="create-button"
                onClick={() => dispatch(openModal())}
              >
                Create Workout
              </button>
            </div>
            <div className="test">
              <Modal
                isOpen={modal}
                onRequestClose={() => dispatch(closeModal())}
                contentLabel="Workout Form Modal"
                style={customStyles}
              >
                <CreateWorkout />
              </Modal>
            </div>
            <div className="test">
              <Modal
                isOpen={editModal}
                onRequestClose={() => dispatch(closeEditModal())}
                contentLabel="Workout Form Modal"
              >
                <UpdateWorkout />
              </Modal>
            </div>
            <div className="test"></div>
          </div>
          <div className="workouts-grid">
            {workouts.map((workout) => (
              <div key={workout.id} className="workout-card">
                <h3>{workout.name}</h3>
                <ul>
                  {workout.exercises.map((exercise, index) => (
                    <li key={index}>{exercise}</li>
                  ))}
                </ul>
                <button
                  className="edit-button"
                  onClick={() => handleEditWorkout(workout)}
                  type="button"
                >
                  Edit
                </button>

                <button
                  className="delete-button"
                  onClick={() => {
                    handleDeleteWorkout(workout.id);
                  }}
                  type="button"
                >
                  Delete
                </button>
              </div>
            ))}
          </div>
        </div>
      </>
    );
  } else {
    return (
      <>
        <div>You don't have any workouts</div>
        <div>
          <button onClick={() => dispatch(openModal())}>Create Workout</button>
        </div>
        <div className="test">
          <Modal
            isOpen={modal}
            onRequestClose={() => dispatch(closeModal())}
            contentLabel="Workout Form Modal"
          >
            <CreateWorkout />
          </Modal>
        </div>
      </>
    );
  }
}

export default ManageWorkouts;
