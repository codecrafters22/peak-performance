steps = [
    [
        # Create the table
        """
        CREATE TABLE notes (
        id SERIAL PRIMARY KEY NOT NULL,
        user_id INT REFERENCES users(id) NOT NULL,
        content TEXT NOT NULL
        );
        """,
        # Drop the table
        """
        DROP TABLE notes;
        """
    ]
]
