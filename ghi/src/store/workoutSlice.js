import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  selectedWorkoutToEdit: null,
};

const workoutSlice = createSlice({
  name: "workout",
  initialState,
  reducers: {
    setSelectedWorkoutToEdit: (state, action) => {
      state.selectedWorkoutToEdit = action.payload;
    },
  },
});

export const { setSelectedWorkoutToEdit } = workoutSlice.actions;

export default workoutSlice.reducer;
