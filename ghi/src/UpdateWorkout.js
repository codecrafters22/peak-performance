import React, { useState, useEffect } from "react";
import { useUpdateWorkoutMutation } from "./store/WorkoutsApi";
import { useSelector, useDispatch } from "react-redux";
import { closeModal } from "./store/modalSlice";
import { closeEditModal } from "./store/editmodalSlice";

function UpdateWorkout() {
  const [workoutName, setWorkoutName] = useState("");
  const [exerciseNames, setExerciseNames] = useState([]);
  const [updateWorkout] = useUpdateWorkoutMutation();
  const dispatch = useDispatch();
  const workout = useSelector((state) => state.workout.selectedWorkoutToEdit);
  const [focusedInput, setFocusedInput] = useState(null);

  const handleWorkoutNameChange = (event) => {
    setWorkoutName(event.target.value);
  };

  const handleExerciseNameChange = (index, event) => {
    const updatedExerciseNames = [...exerciseNames];
    updatedExerciseNames[index] = event.target.value;
    setExerciseNames(updatedExerciseNames);
  };

  const handleAddExercise = () => {
    setExerciseNames([...exerciseNames, ""]);
  };

  const handleDeleteExercise = (index) => {
    const updatedExerciseNames = [...exerciseNames];
    updatedExerciseNames.splice(index, 1);
    setExerciseNames(updatedExerciseNames);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(closeEditModal());
    updateWorkout({
      workout_id: workout.id,
      workoutData: {
        name: workoutName,
        exercises: exerciseNames,
      },
    })
      .unwrap()
      .then((response) => {
        if (response.error) {
          console.error("Failed to update workout:", response.error);
        } else {
          dispatch(closeModal());
        }
      });
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      if (focusedInput === "workoutName") {
        handleSubmit(e);
      } else {
        handleSubmit(e);
      }
    }
  };

  useEffect(() => {
    if (workout) {
      setWorkoutName(workout.name || "");
      setExerciseNames(workout.exercises || []);
    }
  }, [workout]);

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Workout Name"
          value={workoutName}
          onChange={handleWorkoutNameChange}
          onKeyDown={(e) => {
            setFocusedInput("workoutName"); // Track focused input
            handleKeyPress(e);
          }}
        />
        <h3>Exercises</h3>
        {exerciseNames.map((exerciseName, index) => (
          <div key={index}>
            <input
              type="text"
              value={exerciseName}
              onChange={(event) => handleExerciseNameChange(index, event)}
              onKeyDown={(e) => {
                setFocusedInput(`exercise-${index}`);
                handleKeyPress(e);
              }}
            />
            <button onClick={() => handleDeleteExercise(index)}>Delete</button>
          </div>
        ))}
        <button type="button" onClick={handleAddExercise}>
          Add Exercise
        </button>
        <button type="button" onClick={handleSubmit}>
          Submit
        </button>
      </form>
    </div>
  );
}

export default UpdateWorkout;
