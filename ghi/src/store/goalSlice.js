import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  selectedGoalToEdit: null,
};

const goalSlice = createSlice({
  name: "goal",
  initialState,
  reducers: {
    setSelectedGoalToEdit: (state, action) => {
      state.selectedGoalToEdit = action.payload;
    },
  },
});

export const { setSelectedGoalToEdit } = goalSlice.actions;

export default goalSlice.reducer;
