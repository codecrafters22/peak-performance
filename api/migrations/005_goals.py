steps = [
    [
        # Create the table
        """
        CREATE TABLE goals (
        id SERIAL PRIMARY KEY NOT NULL,
        user_id INT REFERENCES users(id) NOT NULL,
        content TEXT NOT NULL,
        status BOOLEAN DEFAULT FALSE
        );
        """,
        # Drop the table
        """
        DROP TABLE goals;
        """
    ]
]
