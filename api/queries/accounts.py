from pydantic import BaseModel
from queries.pool import pool


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str
    weight: str


class AccountOut(BaseModel):
    id: int
    username: str
    weight: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountRepo:
    def create(
            self,
            user: AccountIn,
            hashed_password: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users
                            (username, hashed_password, weight)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,

                        [
                            user.username,
                            hashed_password,
                            user.weight
                        ],
                    )
                    id = result.fetchone()[0]
                    return AccountOutWithPassword(
                        id=id,
                        username=user.username,
                        weight=user.weight,
                        hashed_password=hashed_password
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create User"}

    def get(self, username: str) -> AccountOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, username, hashed_password, weight
                        FROM users
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    user = AccountOutWithPassword(
                        id=record[0],
                        username=record[1],
                        hashed_password=record[2],
                        weight=record[3],
                    )
                    return user
        except Exception:
            return {"message": "Could not get user"}
