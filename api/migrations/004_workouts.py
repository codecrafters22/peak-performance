steps = [
  [
    # Create the table
    """
    CREATE TABLE workouts (
    id SERIAL PRIMARY KEY NOT NULL,
    user_id INT REFERENCES users(id) NOT NULL,
    name VARCHAR(40) NOT NULL,
    exercises TEXT[]
    );
    """,
    # Drop the table
    """
    DROP TABLE workouts;
    """]
]
