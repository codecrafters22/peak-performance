from authenticator import authenticator
from fastapi import APIRouter, Depends
from typing import List
from queries.workouts import WorkoutIn, WorkoutOut, WorkoutRepo


router = APIRouter()


@router.post("/workouts", response_model=WorkoutOut)
async def create_workout(
    workout: WorkoutIn,
    repo: WorkoutRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(workout, account_data)


@router.get("/workouts", response_model=List[WorkoutOut])
async def get_workouts(
    workouts: WorkoutRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return workouts.get_workouts(account_data)


@router.delete("/workouts/{workout_id}", response_model=bool)
async def delete(
    workout_id: int,
    repo: WorkoutRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(workout_id)


@router.put("/workouts/{workout_id}", response_model=WorkoutOut)
def update_workout(
    workout_id: int,
    workout: WorkoutIn,
    repo: WorkoutRepo = Depends(),
) -> WorkoutOut:
    return repo.update(workout_id, workout)
