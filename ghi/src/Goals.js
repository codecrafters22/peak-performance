import React, { useState } from "react";
import "./Goals.css";
import Modal from "react-modal";
import {
  useGetGoalsForAccountQuery,
  useDeleteGoalMutation,
  useUpdateGoalMutation,
} from "./store/WorkoutsApi";
import CreateGoal from "./CreateGoal";
import { useSelector, useDispatch } from "react-redux";
import { openModal, closeModal } from "./store/modalSlice";

Modal.setAppElement("#root");

function Goals() {
  const [goalContent] = useState("");
  const { data: goals, isLoading } = useGetGoalsForAccountQuery();
  const [deleteGoal] = useDeleteGoalMutation();
  const [updateGoal] = useUpdateGoalMutation();
  const modal = useSelector((state) => state.modal.value);
  const dispatch = useDispatch();

  const handleDeleteGoal = (goal_id) => {
    deleteGoal(goal_id);
  };

  const handleEditGoal = (goal_id) => {
    updateGoal({
      goal_id: goal_id,
      goalData: {
        content: goalContent,
      },
    });
  };

  if (isLoading) return <div>Loading...</div>;

  if (goals.length >= 1) {
    return (
      <div className="goals-container">
        <div className="title-contiainer">
          <div className="main-title">Goals</div>
          <div>
            <button
              className="create-button"
              onClick={() => dispatch(openModal())}
            >
              Create Goal
            </button>
          </div>
          <div className="test">
            <Modal
              isOpen={modal}
              onRequestClose={() => dispatch(closeModal())}
              contentLabel="Goal Form Modal"
            >
              <CreateGoal />
            </Modal>
          </div>
        </div>
        <div className="goal-list-container">
          <table className="table">
            <thead>
              <tr>
                <th>Goal</th>
                <th>Status</th>
                <th>Delete</th>
                <th>Mark Complete</th>
              </tr>
            </thead>
            <tbody>
              {goals.map((goal) => {
                return (
                  <tr key={goal.id} value={goal.id}>
                    <td>{goal.content}</td>
                    {!goal.status ? <td>Incomplete</td> : <td>complete</td>}
                    <td>
                      <button
                        className="delete-button"
                        onClick={() => {
                          handleDeleteGoal(goal.id);
                        }}
                        type="button"
                      >
                        Delete
                      </button>
                    </td>
                    <td>
                      <button
                        className="complete-button"
                        onClick={() => handleEditGoal(goal.id)}
                        type="button"
                      >
                        Complete
                      </button>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  } else {
    return (
      <>
        <div>You don't have any goals</div>
        <div>
          <button onClick={() => dispatch(openModal())}>Create Goal</button>
        </div>
        <div className="test">
          <Modal
            isOpen={modal}
            onRequestClose={() => dispatch(closeModal())}
            contentLabel="Workout Form Modal"
          >
            <CreateGoal />
          </Modal>
        </div>
      </>
    );
  }
}

export default Goals;
