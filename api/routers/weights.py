from authenticator import authenticator
from fastapi import APIRouter, Depends
from queries.weights import WeightIn, WeightOut, WeightRepo
from typing import List

router = APIRouter()


@router.post("/weights", response_model=WeightOut)
async def create_weight(
    weight: WeightIn,
    repo: WeightRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(weight, account_data)


@router.get("/weights", response_model=List[WeightOut])
async def get_weights(
    notes: WeightRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return notes.get_weights(account_data)


@router.delete("/weights/{weight_id}", response_model=bool)
async def delete(
    weight_id: int,
    repo: WeightRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(weight_id)
