from fastapi import APIRouter
from pydantic import BaseModel
import requests
from .keys import MYAPIKEY
import json

router = APIRouter()


class QuoteOut(BaseModel):
    quote: str
    author: str


class MuscleIn(BaseModel):
    muscle: str


@router.get("/quote", response_model=QuoteOut)
async def get_quote(

):
    api_url = 'https://api.api-ninjas.com/v1/quotes?category=fitness'
    response = requests.get(api_url, headers={'X-Api-Key': MYAPIKEY})
    if response.status_code == requests.codes.ok:
        content = json.loads(response.content)
        return {
            "quote": content[0]["quote"],
            "author": content[0]["author"]
        }
    else:
        return ("Error:", response.status_code, response.text)
