from pydantic import BaseModel
from queries.pool import pool
from typing import List


class NoteOut(BaseModel):
    id: int
    content: str


class NoteIn(BaseModel):
    content: str


class NotesRepo:
    def create(self, note: NoteIn, account_data: dict) -> NoteOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO notes
                            (content, user_id)
                        VALUES
                            (%s, %s)
                        RETURNING id;
                        """,

                        [
                            note.content,
                            account_data["id"]
                        ],
                    )
                    id = result.fetchone()[0]
                    return NoteOut(
                        id=id,
                        content=note.content
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create note"}

    def get_notes(self, account_data: dict) -> List[NoteOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            user_id,
                            content
                        FROM notes
                        WHERE user_id = %s
                        """,
                        [account_data["id"]],
                    )
                    return [
                        NoteOut(
                            id=record[0],
                            content=record[2],
                        )
                        for record in db
                    ]
        except Exception:
            return {"message": "Could not get all notes"}

    def delete(self, note_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM notes
                        WHERE id = %s
                        """,
                        [note_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False
