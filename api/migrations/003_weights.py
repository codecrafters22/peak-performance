steps = [
    [
        # Create the table
        """
        CREATE TABLE weights (
        id SERIAL PRIMARY KEY NOT NULL,
        user_id INT REFERENCES users(id) NOT NULL,
        weight FLOAT NOT NULL,
        date DATE DEFAULT current_date
        );
        """,
        # Drop the table
        """
        DROP TABLE weights;
        """
    ]
]
