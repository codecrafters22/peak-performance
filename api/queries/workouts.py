from pydantic import BaseModel
from queries.pool import pool
from typing import List


class WorkoutOut(BaseModel):
    id: int
    name: str
    exercises: List[str]


class WorkoutIn(BaseModel):
    name: str
    exercises: List[str]


class WorkoutRepo:
    def create(self, workout: WorkoutIn, account_data: dict) -> WorkoutOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO workouts
                            (name, exercises, user_id)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,

                        [
                            workout.name,
                            workout.exercises,
                            account_data["id"]
                        ],
                    )
                    id = result.fetchone()[0]
                    return WorkoutOut(
                        id=id,
                        name=workout.name,
                        exercises=workout.exercises
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create workout"}

    def get_workouts(self, account_data: dict) -> List[WorkoutOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            user_id,
                            name,
                            exercises
                        FROM workouts
                        WHERE user_id = %s
                        """,
                        [account_data["id"]],
                    )
                    return [
                        WorkoutOut(
                            id=record[0],
                            name=record[2],
                            exercises=record[3]
                        )
                        for record in db
                    ]
        except Exception:
            return {"message": "Could not get all workouts"}

    def delete(self, workout_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM workouts
                        WHERE id = %s
                        """,
                        [workout_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def update(self, workout_id: int, workout: WorkoutIn) -> WorkoutOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE workouts
                        SET name = %s
                            , exercises = %s
                        WHERE id = %s
                        """,
                        [
                            workout.name,
                            workout.exercises,
                            workout_id
                        ]
                    )
                    return self.workout_in_to_out(workout_id, workout)
        except Exception as e:
            print(e)
            return {"message": "Could not update workout"}

    def workout_in_to_out(self, id: int, workout: WorkoutIn):
        old_data = workout.dict()
        return WorkoutOut(id=id, **old_data)
