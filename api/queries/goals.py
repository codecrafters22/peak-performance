from pydantic import BaseModel
from queries.pool import pool
from typing import List


class GoalOut(BaseModel):
    id: int
    content: str
    status: bool


class GoalIn(BaseModel):
    content: str


class GoalRepo:
    def create(self, goal: GoalIn, account_data: dict) -> GoalOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO goals
                            (content, user_id, status)
                        VALUES
                            (%s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            goal.content,
                            account_data["id"],
                            False
                        ],
                    )
                    id = result.fetchone()[0]
                    return GoalOut(
                        id=id,
                        content=goal.content,
                        status=False
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not create goal"}

    def complete(self, goal_id: int) -> GoalOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE goals
                        SET status = TRUE
                        WHERE id = %s
                        """,
                        [
                            goal_id
                        ]
                    )
                    return self.get_goal_by_id(goal_id)
        except Exception as e:
            print(e)
            return {"message": "Could not update goal"}

    def get_goals(self, account_data: dict) -> List[GoalOut]:
        try:
            goals = []
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            content,
                            status
                        FROM goals
                        WHERE user_id = %s
                        """,
                        [account_data["id"]],
                    )
                    for record in db:
                        goals.append(GoalOut(
                            id=record[0],
                            content=record[1],
                            status=record[2],
                        ))
            return goals
        except Exception as e:
            print(e)
            return []

    def get_goal_by_id(self, goal_id: int) -> GoalOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            content,
                            status
                        FROM goals
                        WHERE id = %s
                        """,
                        [goal_id],
                    )
                    record = db.fetchone()
                    if record:
                        return GoalOut(
                            id=record[0],
                            content=record[1],
                            status=record[2],
                        )
                    else:
                        return {"message": "Goal not found"}
        except Exception as e:
            print(e)
            return {"message": "Could not get goal"}

    def delete(self, goal_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM goals
                        WHERE id = %s
                        """,
                        [goal_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False
