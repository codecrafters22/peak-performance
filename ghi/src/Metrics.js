import CreateWeight from "./CreateWeight";
import "./Metrics.css";
import {
  useGetWeightsForAccountQuery,
  useDeleteWeightMutation,
} from "./store/WorkoutsApi";

function Metrics() {
  const { data: weights, isLoading } = useGetWeightsForAccountQuery();
  const [deleteWeight] = useDeleteWeightMutation();
  const handleDeleteWeight = (weight_id) => {
    deleteWeight(weight_id);
  };
  if (isLoading) return <div>Loading...</div>;

  return (
    <div className="main-metrics-container">
      <CreateWeight />
      <table className="table">
        <thead>
          <tr>
            <th>Weight</th>
            <th>Date</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {weights.map((weight) => {
            return (
              <tr key={weight.id} value={weight.id}>
                <td>{weight.weight}</td>
                <td>{weight.date}</td>
                <td>
                  <button
                    className="delete-button"
                    onClick={() => handleDeleteWeight(weight.id)}
                    type="button"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default Metrics;
