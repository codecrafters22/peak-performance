import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { workoutsApi } from "./WorkoutsApi";
import { setupListeners } from "@reduxjs/toolkit/dist/query";
import modalReducer from "./modalSlice";
import workoutReducer from "./workoutSlice";
import editModalReducer from "./editmodalSlice";
import goalReducer from "./goalSlice";

export const store = configureStore({
  reducer: {
    [workoutsApi.reducerPath]: workoutsApi.reducer,
    modal: modalReducer,
    workout: workoutReducer,
    editModal: editModalReducer,
    goal: goalReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(workoutsApi.middleware),
});

setupListeners(store.dispatch);
