import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Login from "./Login";
import Signup from "./Signup";
import Dashboard from "./Dashboard";
import Nav from "./Nav";
import ManageWorkouts from "./ManageWorkouts";
import { useGetTokenQuery } from "./store/WorkoutsApi";
import Metrics from "./Metrics";
import Goals from "./Goals";

function App() {
  const { data: account } = useGetTokenQuery();

  return (
    <BrowserRouter>
      {!account && (
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="login" element={<Login />} />
          <Route path="signup" element={<Signup />} />
        </Routes>
      )}
      {account && (
        <div className="main-div-container">
          <div className="nav-container">
            <Nav />
          </div>
          <div className="display-container">
            <Routes>
              <Route path="/" element={<Dashboard />} />
              <Route path="/workouts" element={<ManageWorkouts />} />
              <Route path="/weights" element={<Metrics />} />
              <Route path="/goals" element={<Goals />} />
            </Routes>
          </div>
        </div>
      )}
    </BrowserRouter>
  );
}

export default App;
