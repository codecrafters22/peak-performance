import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  value: false,
};

export const editModalSlice = createSlice({
  name: "editModal",
  initialState,
  reducers: {
    openEditModal: (state) => {
      state.value = true;
    },
    closeEditModal: (state) => {
      state.value = false;
    },
  },
});

export const { openEditModal, closeEditModal } = editModalSlice.actions;

export default editModalSlice.reducer;
