import React, { useState } from "react";
import { useCreateWorkoutMutation } from "./store/WorkoutsApi";
import { useSelector, useDispatch } from "react-redux";
import { closeModal } from "./store/modalSlice";

function CreateWorkout() {
  const [workoutName, setWorkoutName] = useState("");
  const [exerciseNames, setExerciseNames] = useState([]);
  const [createWorkout] = useCreateWorkoutMutation();
  const modal = useSelector((state) => state.modal.value);
  const dispatch = useDispatch();

  const handleWorkoutNameChange = (event) => {
    setWorkoutName(event.target.value);
  };

  const handleExerciseNameChange = (index, event) => {
    const updatedExerciseNames = [...exerciseNames];
    updatedExerciseNames[index] = event.target.value;
    setExerciseNames(updatedExerciseNames);
  };

  const handleAddExercise = () => {
    setExerciseNames([...exerciseNames, ""]);
  };

  const handleDeleteExercise = (index) => {
    const updatedExerciseNames = [...exerciseNames];
    updatedExerciseNames.splice(index, 1);
    setExerciseNames(updatedExerciseNames);
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleSubmit(e);
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    createWorkout({ name: workoutName, exercises: exerciseNames });
    dispatch(closeModal());
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Workout Name"
          value={workoutName}
          onChange={handleWorkoutNameChange}
        />
        <h3>Exercises</h3>
        {exerciseNames.map((exerciseName, index) => (
          <div key={index}>
            <input
              type="text"
              value={exerciseName}
              onChange={(event) => handleExerciseNameChange(index, event)}
              onKeyDown={(event) => handleKeyPress(event, index)}
            />
            <button type="button" onClick={() => handleDeleteExercise(index)}>
              Delete
            </button>
          </div>
        ))}
        <button type="button" onClick={handleAddExercise}>
          Add Exercise
        </button>
        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default CreateWorkout;
