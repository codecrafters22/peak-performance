import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import "./Dashboard.css";
import {
  useGetWeightsForAccountQuery,
  useGetTokenQuery,
  useGetQuoteQuery,
  useGetNotesForAccountQuery,
  useDeleteNoteMutation,
  useUpdateGoalMutation,
  useGetGoalsForAccountQuery,
  useGetWorkoutsForAccountQuery,
} from "./store/WorkoutsApi";
import CreateNote from "./CreateNote";
import CreateGoal from "./CreateGoal";

function Dashboard() {
  const { data } = useGetTokenQuery();
  const username = data.username;
  const { data: quote, isError: isQuoteError } = useGetQuoteQuery();
  const { data: notes, isLoading: isNotesLoading } =
    useGetNotesForAccountQuery();
  const {
    data: weights,
    isLoading: isWeightsLoading,
    isError: isWeightsError,
  } = useGetWeightsForAccountQuery();
  const { data: workouts } = useGetWorkoutsForAccountQuery();
  const [deleteNote, deleteNoteResponse] = useDeleteNoteMutation();
  const [goalContent, setGoalContent] = useState("");
  const [updateGoal] = useUpdateGoalMutation();
  const { data: goals, isLoading, isError } = useGetGoalsForAccountQuery();
  const handleDeleteNote = (note_id) => {
    deleteNote(note_id);
  };
  const handleEditGoal = (goal_id) => {
    updateGoal({
      goal_id: goal_id,
      goalData: {
        content: goalContent,
      },
    });
  };
  if (isWeightsLoading || isNotesLoading) return <div>Loading...</div>;
  return (
    <div className="main-container">
      <div className="header-container">
        <div className="dashboard-greeting">Welcome back, {username}</div>
      </div>
      <div className="body-container">
        <div className="dashboard-grid">
          <NavLink className="dashboard-link" to="/workouts">
            <div className="grid-item workouts-item">
              <div className="grid-item-title">Workouts</div>
              <div className="grid-item-body">
                <div className="grid-item-icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="40"
                    height="40"
                    fill="currentColor"
                    class="bi bi-heart-pulse"
                    viewBox="0 0 16 16"
                  >
                    <path d="m8 2.748-.717-.737C5.6.281 2.514.878 1.4 3.053.918 3.995.78 5.323 1.508 7H.43c-2.128-5.697 4.165-8.83 7.394-5.857.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17c3.23-2.974 9.522.159 7.394 5.856h-1.078c.728-1.677.59-3.005.108-3.947C13.486.878 10.4.28 8.717 2.01L8 2.748ZM2.212 10h1.315C4.593 11.183 6.05 12.458 8 13.795c1.949-1.337 3.407-2.612 4.473-3.795h1.315c-1.265 1.566-3.14 3.25-5.788 5-2.648-1.75-4.523-3.434-5.788-5Z" />
                    <path d="M10.464 3.314a.5.5 0 0 0-.945.049L7.921 8.956 6.464 5.314a.5.5 0 0 0-.88-.091L3.732 8H.5a.5.5 0 0 0 0 1H4a.5.5 0 0 0 .416-.223l1.473-2.209 1.647 4.118a.5.5 0 0 0 .945-.049l1.598-5.593 1.457 3.642A.5.5 0 0 0 12 9h3.5a.5.5 0 0 0 0-1h-3.162l-1.874-4.686Z" />
                  </svg>
                </div>
                <div className="grid-item-content">
                  {workouts ? workouts.length : 0}
                </div>
              </div>
            </div>
          </NavLink>
          <NavLink className="dashboard-link" to="/goals">
            <div className="grid-item goals-item">
              <div className="grid-item-title">Goals</div>
              <div className="grid-item-body">
                <div className="grid-item-icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="40"
                    height="40"
                    fill="currentColor"
                    class="bi bi-list-check"
                    viewBox="0 0 16 16"
                  >
                    <path
                      fill-rule="evenodd"
                      d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"
                    />
                  </svg>
                </div>
                <div className="grid-item-content">
                  {goals ? goals.length : 0}
                </div>
              </div>
            </div>
          </NavLink>
          <NavLink className="dashboard-link" to="/weights">
            <div className="grid-item weight-item">
              <div className="grid-item-title">Go to Metrics</div>
              <div className="grid-item-body">
                <div className="grid-item-icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="40"
                    height="40"
                    fill="currentColor"
                    class="bi bi-clipboard-data"
                    viewBox="0 0 16 16"
                  >
                    <path d="M4 11a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0v-1zm6-4a1 1 0 1 1 2 0v5a1 1 0 1 1-2 0V7zM7 9a1 1 0 0 1 2 0v3a1 1 0 1 1-2 0V9z" />
                    <path d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z" />
                    <path d="M9.5 1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5v-1a.5.5 0 0 1 .5-.5h3zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z" />
                  </svg>
                </div>
                <div className="grid-item-content"></div>
              </div>
            </div>
          </NavLink>
          <div className="grid-item-2 quote-item">
            <div className="grid-item-2-title">Quote of The Day</div>
            <div className="quote-body">
              <div className="quote">{quote.quote}</div>
              <div className="author">{quote.author}</div>
            </div>
          </div>
          <div className="grid-item-3 note-item">
            <div className="grid-item-3-title">
              <CreateNote />
              <table className="notes-table table">
                <thead>
                  <tr>
                    <th>Notes</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {notes.map((note) => {
                    return (
                      <tr key={note.id} value={note.id}>
                        <td>{note.content}</td>
                        <td>
                          <button
                            className="delete-button"
                            onClick={() => handleDeleteNote(note.id)}
                            type="button"
                          >
                            Delete
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Dashboard;
