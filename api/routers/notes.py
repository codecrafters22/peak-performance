from authenticator import authenticator
from fastapi import APIRouter, Depends
from queries.notes import NoteOut, NoteIn, NotesRepo
from typing import List


router = APIRouter()


@router.post("/notes", response_model=NoteOut)
async def create_note(
    note: NoteIn,
    repo: NotesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.create(note, account_data)


@router.get("/notes", response_model=List[NoteOut])
async def get_notes(
    notes: NotesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return notes.get_notes(account_data)


@router.delete("/notes/{note_id}", response_model=bool)
async def delete(
    note_id: int,
    repo: NotesRepo = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return repo.delete(note_id)
