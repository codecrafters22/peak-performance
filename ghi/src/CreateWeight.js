import { useCreateWeightMutation } from "./store/WorkoutsApi";
import React, { useState } from "react";
import "./CreateWeight.css";

function CreateWeight() {
  const [createWeight] = useCreateWeightMutation();
  const [weightValue, setWeightValue] = useState(0.0);

  const handleSubmit = (e) => {
    e.preventDefault();
    createWeight({ weight: weightValue });
  };

  const handleWeightValueChange = (event) => {
    setWeightValue(event.target.value);
  };

  return (
    <div>
      <div className="main-title">Track your weight</div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Weight"
          value={weightValue}
          onChange={handleWeightValueChange}
        />
        <button className="submit-button" type="submit">
          Submit
        </button>
      </form>
    </div>
  );
}

export default CreateWeight;
