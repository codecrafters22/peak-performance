import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const workoutsApi = createApi({
  reducerPath: "workouts",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_HOST,
  }),
  endpoints: (builder) => ({
    getWorkouts: builder.query({
      query: () => "/workouts",
    }),
    getWorkoutsForAccount: builder.query({
      query: () => ({
        url: "/workouts",
        credentials: "include",
      }),
      providesTags: ["Workouts"],
    }),
    getGoalsForAccount: builder.query({
      query: () => ({
        url: "/goals",
        credentials: "include",
      }),
      providesTags: ["Goals"],
    }),
    getWeightsForAccount: builder.query({
      query: () => ({
        url: "/weights",
        credentials: "include",
      }),
      providesTags: ["Weights"],
    }),
    createWorkout: builder.mutation({
      query: (workoutData) => ({
        url: "/workouts",
        method: "POST",
        body: workoutData,
        credentials: "include",
      }),
      invalidatesTags: ["Workouts"],
    }),
    createGoal: builder.mutation({
      query: (goalData) => ({
        url: "/goals",
        method: "POST",
        body: goalData,
        credentials: "include",
      }),
      invalidatesTags: ["Goals"],
    }),
    createWeight: builder.mutation({
      query: (weightData) => ({
        url: "/weights",
        method: "POST",
        body: weightData,
        credentials: "include",
      }),
      invalidatesTags: ["Weights"],
    }),
    createNote: builder.mutation({
      query: (NoteData) => ({
        url: "/notes",
        method: "POST",
        body: NoteData,
        credentials: "include",
      }),
      invalidatesTags: ["Notes"],
    }),
    getNotesForAccount: builder.query({
      query: () => ({
        url: "/notes",
        credentials: "include",
      }),
      providesTags: ["Notes"],
    }),
    deleteNote: builder.mutation({
      query: (note_id) => ({
        url: `/notes/${note_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Notes"],
    }),
    deleteGoal: builder.mutation({
      query: (goal_id) => ({
        url: `/goals/${goal_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Goals"],
    }),
    deleteWorkout: builder.mutation({
      query: (workout_id) => ({
        url: `/workouts/${workout_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Workouts"],
    }),
    updateWorkout: builder.mutation({
      query: ({ workout_id, workoutData }) => ({
        url: `/workouts/${workout_id}`,
        method: "PUT",
        body: workoutData,
        credentials: "include",
      }),
      invalidatesTags: ["Workouts"],
    }),
    updateGoal: builder.mutation({
      query: ({ goal_id, goalData }) => ({
        url: `/goals/${goal_id}`,
        method: "PUT",
        body: goalData,
        credentials: "include",
      }),
      invalidatesTags: ["Goals"],
    }),
    deleteWeight: builder.mutation({
      query: (weight_id) => ({
        url: `/weights/${weight_id}`,
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Weights"],
    }),
    getToken: builder.query({
      query: () => ({
        url: "/token",
        credentials: "include",
      }),
      transformResponse: (response) => response?.account || null,
      providesTags: ["Account"],
    }),
    getQuote: builder.query({
      query: () => "/quote",
    }),
    logout: builder.mutation({
      query: () => ({
        url: "/token",
        method: "DELETE",
        credentials: "include",
      }),
      invalidatesTags: ["Account", "Workouts"],
    }),
    login: builder.mutation({
      query: (info) => {
        const formData = new FormData();
        formData.append("username", info.username);
        formData.append("password", info.password);
        return {
          url: "/token",
          method: "POST",
          body: formData,
          credentials: "include",
        };
      },
      invalidatesTags: ["Account", "Workouts"],
    }),
    signup: builder.mutation({
      query: (body) => ({
        url: "/accounts",
        method: "POST",
        body,
        credentials: "include",
      }),
      invalidatesTags: ["Account", "Workouts"],
    }),
  }),
});
export const {
  useGetWorkoutsQuery,
  useGetTokenQuery,
  useLogoutMutation,
  useGetWorkoutsForAccountQuery,
  useLoginMutation,
  useSignupMutation,
  useDeleteWorkoutMutation,
  useCreateWorkoutMutation,
  useUpdateWorkoutMutation,
  useGetWeightsForAccountQuery,
  useCreateWeightMutation,
  useDeleteWeightMutation,
  useGetQuoteQuery,
  useCreateNoteMutation,
  useDeleteNoteMutation,
  useGetNotesForAccountQuery,
  useGetGoalsForAccountQuery,
  useCreateGoalMutation,
  useDeleteGoalMutation,
  useUpdateGoalMutation,
} = workoutsApi;
