from pydantic import BaseModel
from datetime import date
from queries.pool import pool
from typing import List


class WeightOut(BaseModel):
    id: int
    weight: float
    date: date


class WeightIn(BaseModel):
    weight: float


class WeightRepo:
    def create(self, weight: WeightIn, account_data: dict) -> WeightOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO weights
                            (weight, user_id)
                        VALUES
                            (%s, %s)
                        RETURNING id;
                        """,
                        [
                            weight.weight,
                            account_data["id"]
                        ],
                    )
                    id = result.fetchone()[0]
                    return WeightOut(
                        id=id,
                        weight=weight.weight,
                        date=date.today()
                    )
        except Exception as e:
            print(e)
            return {"message": "Could not add weight"}

    def get_weights(self, account_data: dict) -> List[WeightOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            user_id,
                            weight,
                            date
                        FROM weights
                        WHERE user_id = %s
                        """,
                        [account_data["id"]],
                    )
                    return [
                        WeightOut(
                            id=record[0],
                            weight=record[2],
                            date=record[3]
                        )
                        for record in db
                    ]
        except Exception as e:
            print(e)
            return {"message": "Can't get weights"}

    def delete(self, weight_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM weights
                        WHERE id = %s
                        """,
                        [weight_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False
