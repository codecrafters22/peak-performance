import React from "react";
import "./Login.css";
import img1 from "./static/images/login-photo.jpg";
import { useLoginMutation } from "./store/WorkoutsApi";
import { useNavigate, NavLink } from "react-router-dom";
import { useEffect, useState } from "react";

function Login() {
  const navigate = useNavigate();
  const [login, loginResponse] = useLoginMutation();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    if (loginResponse.isSuccess) navigate("/");
    if (loginResponse.isError) {
      setErrorMessage(loginResponse.error.data.detail);
    }
  }, [loginResponse]);

  const handleSubmit = (e) => {
  e.preventDefault();

  if (!password) {
    setErrorMessage("Please enter a password.");
  } else {
    login({ username, password });
  }
};


  return (
    <div className="main-login-container">
      <div className="login-container">
        <div className="login-image-container">
          <img className="login-image" src={img1}></img>
        </div>
        <div className="login-form-container">
          <h3>Welcome</h3>
          {errorMessage && <div className="alert-message">{errorMessage}</div>}
          <form onSubmit={handleSubmit} className="login-form">
            <input
              placeholder="Username"
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            ></input>
            <input
              placeholder="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            ></input>
            <button
              onClick={() => login}
              className="login-button"
              type="submit"
            >
              Login
            </button>
            <div>
              <p>
                Don't have an account? <NavLink to="/signup">Sign Up</NavLink>
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
